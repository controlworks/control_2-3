import ModCalcPi
import ModChart


def processor():
    """Главная функция, через которую производятся вычисления числа Pi С разным количеством числа испытаний"""

    results = []
    indices = []
    end_range = 5000000
    current_range = 50
    index = 0
    while current_range <= end_range:
        res = ModCalcPi.calc_p(current_range)
        results.append(res)
        indices.append(index)
        current_range *= 10
        index += 1

    ModChart.show_chart(indices, results)


processor()
