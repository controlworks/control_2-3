import random

def calc_p(range_value):
    g = 0.0
    numb_pi = 0
    for i in range(range_value):
        x = random.random()
        y = random.random()
        g += (x * x + y * y < 1.0)
        numb_pi = 4 * g / range_value
    return numb_pi