import matplotlib.pyplot as plt

def show_chart(indices, results):
    width = 0.35

    fig, ax = plt.subplots()

    ax.plot(indices, results, width)

    ax.set_ylabel('Y ')
    ax.set_xlabel('X')
    ax.set_title('PI Monte Carlo')

    plt.show()